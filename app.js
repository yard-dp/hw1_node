const express = require('express')
const app = express();
const filesRoutes = require('./routes/files');

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use('/api/files', filesRoutes.routes)
app.use('/', (req,res) => {
    res.status(500).json({message: 'Server Error'})
})
app.listen('8080', () => {
    console.log('Server started on port 8080')
})