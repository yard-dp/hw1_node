const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const uuid = require('uuid');
const directoryPath = path.join(__dirname, '../filefolder');
const JsonPath = path.join(__dirname, '../Files.json')

router.get('/', (req, res) => {
    req.requestId = uuid.v1();
    console.info(`[${req.requestId}] ${req.method} ${req.originalUrl}`)

    try {
        if (!fs.existsSync(directoryPath)) {
          fs.mkdirSync(directoryPath)
        }
      } catch (err) {
        console.error(err)
      }
      
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            console.log('Unable to scan directory: ' + err);
            res.status(500).json({message: 'Server Error'})
            return res.end()
        } else {
            if (!files.length) {
                console.log('No files in Directory!')
                res.status(400).json({message: "Client error"})
                return res.end()
            }
            const myFiles = [];
            files.forEach(function (file) {
                console.log(file);
                myFiles.push(file);
            });
            res.status(200).json({message: 'Success',files: myFiles})
            return res.end()
        }
    });
})
router.post('/', (req, res) => {
    req.requestId = uuid.v1();
    console.info(`[${req.requestId}] ${req.method} ${req.originalUrl}`)
    try {
        if (!fs.existsSync(directoryPath)) {
          fs.mkdirSync(directoryPath)
        }
      } catch (err) {
        console.error(err)
    }
    if (!req.body.filename || !req.body.content) {
        res.status(400).json({message: "Please specify 'content' parameter"})
        return res.end()
    }
    if (fs.existsSync(path.join(directoryPath, req.body.filename))) {
        console.log('File already Created');
        res.status(500).json({message: "Server Error"})
        return res.end()
    }
    fs.writeFile(path.join(directoryPath, req.body.filename), req.body.content, (err) => {
        if (err) {
            console.log('Something went wrong');
            res.status(500).json({message: "Server Error"})
            return res.end()
        }
        const {birthtime} = fs.statSync(path.join(directoryPath, req.body.filename))
        const newFile = {
            filename: req.body.filename,
            content: req.body.content,
            extension: req.body.filename.split('.').pop(),
            uploadedDate: birthtime
        }
        fs.readFile(JsonPath, 'utf-8', (err, data) => {
            if (err) {
                let filesObj = {
                    files: []
                };
                filesObj.files.push(newFile)
                let dataJson = JSON.stringify(filesObj, null, 2);
                fs.writeFile(JsonPath, dataJson, 'utf-8', (err) => {
                    if (err) {
                        res.status(500).json({message: "Server Error"})
                        return res.end()
                    }
                    res.status(200).json({message: "File created successfully"})
                    return res.end()
                })
            } else {
                let obj = JSON.parse(data);
                obj.files.push(newFile)
                let dataJson = JSON.stringify(obj, null, 2);
                fs.writeFile(JsonPath, dataJson, 'utf-8', (err) => {
                    if (err) {
                        res.status(500).json({message: "Server Error"})
                        return res.end()
                    }
                    res.status(200).json({message: "File created successfully"})
                    return res.end()
                })
            }
        })
    })

})
router.get('/:filename', (req, res) => {
    try {
        if (!fs.existsSync(directoryPath)) {
          fs.mkdirSync(directoryPath)
        }
      } catch (err) {
        console.error(err)
      }
    fs.readFile(path.join(directoryPath, req.params.filename), (err) => {
        if (err) {
            console.log("Can't read file")
            res.status(400).json({message: `No file with ${req.params.filename} filename found`})
            return res.end()
        }
        fs.readFile(JsonPath, 'utf-8', (err,data) => {
            if (err) {
                res.status(500).json({message: "Server Error"})
                return res.end()
            } else {
                let obj = JSON.parse(data);
                const file = obj.files.filter(file => file.filename === req.params.filename);
                res.status(200).json({message:'Success',filename: file[0].filename, content: file[0].content,extension: file[0].extension, uploadedDate: file[0].uploadedDate});
                return res.end()
            }
        })
    })
})
exports.routes = router;